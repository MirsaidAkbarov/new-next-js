import Link from "next/link"

const Post = ({ post }) => {
    return (
        <div>
            <Link href="/">Back</Link>
            <h1>{post.title}</h1>
            <p>{post.body}</p>
            <strong>Avtor ID: {post.id}</strong>
        </div>
    )
}
export default Post