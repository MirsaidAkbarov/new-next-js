
import Link from "next/link";
import styles from './style.css'

const Header = () => {
    return (
        <header className={styles.header}>
            <span className={styles.logo}>itProger</span>
            <nav className={styles.nav}>
                <Link href="/">Home</Link>
                <Link href="/about">About</Link>
                <Link href="/calculator">Calculator</Link>
                <Link href="/todoapp">Todo App</Link>
            </nav>
        </header>
    )
}
export default Header;