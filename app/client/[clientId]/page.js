export default function ClientId({ params }) {
    return (
        <div>
            <h1>Client Id Page</h1>
            <p>{params.clientId}</p>
        </div>
    )
  }