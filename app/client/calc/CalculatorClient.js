'use client'

import React from 'react';
import { Provider } from 'react-redux';
import { useDispatch, useSelector } from 'react-redux';
import { ACTIONS } from './calculatorRedux';
import { store } from './calculatorRedux';
import styles from './styles.module.scss';

const CalculatorClient = () => {
  const dispatch = useDispatch();
  const { result, input } = useSelector((state) => state);

  const handleNumberClick = (num) => {
    dispatch({ type: ACTIONS.SET_INPUT, payload: input + num });
  };

  const handleOperatorClick = (operator) => {
    if (input !== '' && !input.endsWith('.') && !input.endsWith('(')) {
      dispatch({ type: ACTIONS.SET_INPUT, payload: input + operator });
    }
  };

  const handleEqualsClick = () => {
    try {
      const evalResult = eval(input);
      dispatch({ type: ACTIONS.SET_RESULT, payload: evalResult });
      dispatch({ type: ACTIONS.SET_INPUT, payload: evalResult.toString() });
    } catch (error) {
      dispatch({ type: ACTIONS.SET_RESULT, payload: 'Error' });
    }
  };

  const handleClearClick = () => {
    dispatch({ type: ACTIONS.CLEAR });
  };

  const handleDeleteClick = () => {
    dispatch({ type: ACTIONS.SET_INPUT, payload: input.slice(0, -1) });
  };

  const handleLastAnswerClick = () => {
    dispatch({ type: ACTIONS.SET_INPUT, payload: input + result });
  };

  const handleDotClick = () => {
    const lastChar = input.slice(-1);
    if (!input.includes('.') && lastChar !== '(') {
      dispatch({ type: ACTIONS.SET_INPUT, payload: input + '.' });
    }
  };



  const handleOpenBracketClick = () => {
    const lastChar = input.slice(-1);
    if (lastChar === '' || lastChar === '+' || lastChar === '-' || lastChar === '*' || lastChar === '/') {
      dispatch({ type: ACTIONS.SET_INPUT, payload: input + '(' });
    }
  };
  

  const handleCloseBracketClick = () => {
    const lastChar = input.slice(-1);
    const openBracketCount = (input.match(/\(/g) || []).length;
    const closeBracketCount = (input.match(/\)/g) || []).length;

    if (openBracketCount > closeBracketCount && !lastChar.match(/[+\-*/.]/)) {
      dispatch({ type: ACTIONS.SET_INPUT, payload: input + ')' });
    }
  };

  const handlePlusMinusClick = () => {
    dispatch({ type: ACTIONS.SET_INPUT, payload: input.startsWith('-') ? input.slice(1) : `-${input}` });
  };

  const handleDoubleZeroClick = () => {
    dispatch({ type: ACTIONS.SET_INPUT, payload: input + '00' });
  };

  return (
    <div className={styles.calculator}>
      <div className={styles.result}>
        <p className={styles.input}>{input}</p>
      </div>  
      <div className={styles.buttonRow}>
        <button className={`${styles.button} ${styles.operator}`} onClick={handleOpenBracketClick}>(</button>
        <button className={`${styles.button} ${styles.operator}`} onClick={handleDotClick}>.</button>
        <button className={`${styles.button} ${styles.operator}`} onClick={handleCloseBracketClick}>)</button>
        <button className={`${styles.button} ${styles.delete}`} onClick={handleDeleteClick}>DEL</button>
      </div>
      <div className={styles.buttonRow}>
        <button className={styles.button} onClick={() => handleNumberClick('7')}>7</button>
        <button className={styles.button} onClick={() => handleNumberClick('8')}>8</button>
        <button className={styles.button} onClick={() => handleNumberClick('9')}>9</button>
        <button className={`${styles.button} ${styles.operator}`} onClick={() => handleOperatorClick('*')}>×</button>
      </div>
      <div className={styles.buttonRow}>
        <button className={styles.button} onClick={() => handleNumberClick('4')}>4</button>
        <button className={styles.button} onClick={() => handleNumberClick('5')}>5</button>
        <button className={styles.button} onClick={() => handleNumberClick('6')}>6</button>
        <button className={`${styles.button} ${styles.operator}`} onClick={() => handleOperatorClick('-')}>−</button>
      </div>
      <div className={styles.buttonRow}>
        <button className={styles.button} onClick={() => handleNumberClick('1')}>1</button>
        <button className={styles.button} onClick={() => handleNumberClick('2')}>2</button>
        <button className={styles.button} onClick={() => handleNumberClick('3')}>3</button>
        <button className={`${styles.button} ${styles.operator}`} onClick={() => handleOperatorClick('+')}>+</button>
      </div>
      <div className={styles.buttonRow}>
      <button className={styles.button} onClick={handleDoubleZeroClick}>00</button>
        <button className={styles.button} onClick={() => handleNumberClick('0')}>0</button>
        <button className={`${styles.button} ${styles.operator}`} onClick={handlePlusMinusClick}>±</button>

        <button className={`${styles.button} ${styles.operator}`} onClick={() => handleOperatorClick('/')}>÷</button>
      </div>
      <div className={styles.buttonRow}>
        <button className={`${styles.button} ${styles.lastAnswer}`} onClick={handleLastAnswerClick}>Ans</button>
                <button className={`${styles.button} ${styles.clear}`} onClick={handleClearClick}>C</button>

        <button className={`${styles.button} ${styles.equals}`} onClick={handleEqualsClick}>=</button>

      </div>
    </div>
  );
};

const WrappedCalculatorClient = () => (
  <Provider store={store}>
    <CalculatorClient />
  </Provider>
);

export default WrappedCalculatorClient;