import { createStore } from 'redux';


export const ACTIONS = {
  SET_INPUT: 'SET_INPUT',
  SET_RESULT: 'SET_RESULT',
  CLEAR: 'CLEAR',
};

export const initialState = {
  result: 0,
  input: '',
};

export const calculatorReducer = (state = initialState, action) => {
  switch (action.type) {
    case ACTIONS.SET_INPUT:
      return { ...state, input: action.payload };
    case ACTIONS.SET_RESULT:
      return { ...state, result: action.payload };
    case ACTIONS.CLEAR:
      return { ...initialState };
    default:
      return state;
  }
  
};

export const store = createStore(calculatorReducer);
