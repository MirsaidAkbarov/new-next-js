"use client";

import React, { useState, useEffect } from 'react';
import styles from './styles.module.scss';

async function fetchData() {
    const res = await fetch('https://jsonplaceholder.typicode.com/posts');
    const result = await res.json();
    return result;
}

const TodoApp = () => {
    const [todos, setTodos] = useState([]);
    const [inputValue, setInputValue] = useState('');

    useEffect(() => {
        const fetchTodos = async () => {
            const data = await fetchData();
            const formattedData = data.slice(0, 5).map(post => ({ id: post.id, title: post.title }));
            setTodos(formattedData);
        };

        fetchTodos(); 
    }, []); 

    const handleInputChange = (e) => {
        setInputValue(e.target.value);
    };

    const handleAddTodo = () => {
        if (inputValue.trim() !== '') {
            setTodos([...todos, { id: todos.length + 1, title: inputValue }]);
            setInputValue('');
        }
    };

    const handleEditTodo = (index) => {
        const newTodos = [...todos];
        const updatedTodo = prompt('Enter the updated todo:');
        if (updatedTodo !== null) {
            newTodos[index].title = updatedTodo;
            setTodos(newTodos);
        }
    };

    const handleDeleteTodo = (index) => {
        const newTodos = [...todos];
        newTodos.splice(index, 1);
        setTodos(newTodos);
    };

    const handleClearAll = () => {
        setTodos([]);
    };

    const handleKeyDown = (e) => {
        if (e.key === 'Enter') {
            handleAddTodo();
        }
    };

    return (
        <div className={styles.wrapper}>
            <h1 className={styles.title}>Todo App</h1>
            <input
                type="text"
                value={inputValue}
                onChange={handleInputChange}
                onKeyDown={handleKeyDown}
                className={styles.input}
            />
            <button onClick={handleAddTodo} className={styles.button}>Add Todo</button>
            <ul className={styles.list}>
                {todos.map((todo, index) => (
                    <li key={index} className={styles.item}>
                        {todo.title}
                        <div className={styles.divider} >
                            <button onClick={() => handleEditTodo(index)} className={styles.button}>Edit</button>
                            <button onClick={() => handleDeleteTodo(index)} className={styles.button}>Delete</button>
                        </div>
                    </li>
                ))}
            </ul>
            <button onClick={handleClearAll} className={styles.button}>Clear All</button>
        </div>
    );
};

export default TodoApp;
