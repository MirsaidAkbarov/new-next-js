import Link from 'next/link'
import styles from "./styles.module.scss"


async function fetchData() {
  const res = await fetch('https://jsonplaceholder.typicode.com/posts')
  const result = await res.json()
  return result
}

export default async function Home() {

  const post = await fetchData()

  return (
    <div className={styles.wrapper}>
         <h1 className={styles.title}>home Page</h1>
         {post.map((item) => (
           <div key={item.id} className={styles.post}>
             <h2>{item.title}</h2>
             <p>{item.body}</p>
              <Link href={`/post/${item.id}`}>Read more</Link>
           </div>
         ))}
    </div>
  )
}
