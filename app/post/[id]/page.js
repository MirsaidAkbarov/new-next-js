
import Post from '@/app/component/post'

async function fetchData(id) {
    const res = await fetch('https://jsonplaceholder.typicode.com/posts/' + id)
    const result = await res.json()
    return result
}


const PostJs = async ({ params: { id } }) => {
    const post = await fetchData(id)
    return (
        <div>
            <Post post = {post} />
        </div>
    )
}

export default PostJs