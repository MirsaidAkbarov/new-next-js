import React from 'react';
import dynamic from 'next/dynamic';

const CalculatorClient = dynamic(() => import('../client/calc/CalculatorClient'), {
  ssr: false, });

const Calc = () => {
  return (
    <div>
      <h1>Calculator</h1>
      <CalculatorClient />
    </div>
  );
};

export default Calc;